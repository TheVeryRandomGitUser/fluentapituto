﻿using FakeItEasy;
using FluentApiTuto.Part2;
using FluentApiTuto.Part4.Services;
using NUnit.Framework;

namespace FluentApiTuto.Part4.Tests
{
    [TestFixture]
    public class OrderProcessorTests
    {
        private IProduct fakeProduct;
        private IPackage fakePackage;
        private IStorageService fakeStorageService;
        private IShippingService fakeShippingService;
        private IAuditService fakeAuditService;
        private OrderProcessor orderProcessor;

        [SetUp]
        public void SetUp()
        {
            fakeProduct = A.Fake<IProduct>();

            fakePackage = A.Fake<IPackage>();

            fakeStorageService = A.Fake<IStorageService>();
            A.CallTo(() => fakeStorageService.Fetch(A<IProduct>._, A<int>._)).Returns(fakePackage);

            fakeShippingService = A.Fake<IShippingService>();

            fakeAuditService = A.Fake<IAuditService>();

            orderProcessor = new OrderProcessor(fakeStorageService, fakeShippingService, fakeAuditService);
        }

        [Test]
        public void T001()
        {
            // Given
            var product = fakeProduct;
            /*
             * Order 
             * the product
             */
            Order order = Orders.Order();// TODO

            // When
            orderProcessor.Process(order);
            // Then
            A.CallTo(() => fakeStorageService.Fetch(product, 1))
                .MustHaveHappened(Repeated.Exactly.Once);
        }

        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        public void T002(int quantity)
        {
            // Given
            var product = fakeProduct;
            /*
             * Order 
             * [quantity] pieces of
             * the product
             */
            Order order = Orders.Order();// TODO

            // When
            orderProcessor.Process(order);
            // Then
            A.CallTo(() => fakeStorageService.Fetch(product, quantity))
                .MustHaveHappened(Repeated.Exactly.Once);
        }

        [TestCase(ShippingMethod.Normal)]
        [TestCase(ShippingMethod.Express)]
        [TestCase(ShippingMethod.Courier)]
        public void T003(ShippingMethod shippingMethod)
        {
            // Given
            /*
             * Order using [shippingMethod]
             */
            Order order = Orders.Order();// TODO

            // When
            orderProcessor.Process(order);
            // Then
            A.CallTo(() => fakeShippingService.Ship(A<IPackage>._, shippingMethod))
                .MustHaveHappened(Repeated.Exactly.Once);
        }

        [Test]
        public void T004()
        {
            // Given
            /*
             * Any valid order
             */
            Order order = Orders.Order();// TODO

            // When
            orderProcessor.Process(order);
            // Then
            A.CallTo(() => fakeAuditService.NotifySent(A<IPackage>._))
                .MustHaveHappened(Repeated.Exactly.Once);
        }

        [Test]
        public void T005()
        {
            // Given
            /*
             * Order not insured shipment
             */
            Order order = Orders.Order();// TODO

            // When
            orderProcessor.Process(order);
            // Then
            A.CallTo(() => fakeAuditService.NotifyInsured(A<IPackage>._))
                .MustNotHaveHappened();
        }

        [Test]
        public void T006()
        {
            // Given
            /*
             * Order insured shipment
             */
            Order order = Orders.Order();// TODO

            // When
            orderProcessor.Process(order);
            // Then
            A.CallTo(() => fakeAuditService.NotifyInsured(A<IPackage>._))
                .MustHaveHappened(Repeated.Exactly.Once);
        }

        [Test]
        public void T007()
        {
            // Given
            var product = fakeProduct;
            Order order = Orders.Order();// TODO

            // When
            orderProcessor.Process(order);
            // Then
            A.CallTo(() => fakeStorageService.Fetch(product, 1))
                .MustHaveHappened(Repeated.Exactly.Once);
            A.CallTo(() => fakeShippingService.Ship(A<IPackage>._, ShippingMethod.Normal))
                .MustHaveHappened(Repeated.Exactly.Once);
            A.CallTo(() => fakeAuditService.NotifyInsured(A<IPackage>._))
                .MustNotHaveHappened();
            A.CallTo(() => fakeAuditService.NotifySent(A<IPackage>._))
                .MustHaveHappened(Repeated.Exactly.Once);
        }

        [Test]
        public void T008()
        {
            // Given
            var product = fakeProduct;
            Order order = Orders.Order();// TODO

            // When
            orderProcessor.Process(order);
            // Then
            A.CallTo(() => fakeStorageService.Fetch(product, 666))
                .MustHaveHappened(Repeated.Exactly.Once);
            A.CallTo(() => fakeShippingService.Ship(A<IPackage>._, ShippingMethod.Express))
                .MustHaveHappened(Repeated.Exactly.Once);
            A.CallTo(() => fakeAuditService.NotifyInsured(A<IPackage>._))
                .MustHaveHappened(Repeated.Exactly.Once);
            A.CallTo(() => fakeAuditService.NotifySent(A<IPackage>._))
                .MustHaveHappened(Repeated.Exactly.Once);
        }

        [Test]
        public void T009()
        {
            // Given
            var product = fakeProduct;
            var orders = new Order[]
            {
                Orders.Order(), // TODO
                Orders.Order(), // TODO
            };

            // When
            foreach (var order in orders)
            {
                orderProcessor.Process(order);
            }
            // Then
            A.CallTo(() => fakeStorageService.Fetch(product, 1))
                .MustHaveHappened(Repeated.Exactly.Twice);
            A.CallTo(() => fakeShippingService.Ship(A<IPackage>._, ShippingMethod.Normal))
                .MustHaveHappened(Repeated.Exactly.Once);
            A.CallTo(() => fakeShippingService.Ship(A<IPackage>._, ShippingMethod.Express))
                .MustHaveHappened(Repeated.Exactly.Once);
            A.CallTo(() => fakeAuditService.NotifyInsured(A<IPackage>._))
                .MustHaveHappened(Repeated.Exactly.Once);
            A.CallTo(() => fakeAuditService.NotifySent(A<IPackage>._))
                .MustHaveHappened(Repeated.Exactly.Twice);
        }
    }
}