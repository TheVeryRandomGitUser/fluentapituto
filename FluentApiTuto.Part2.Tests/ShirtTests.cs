﻿using System.Drawing;
using NUnit.Framework;

namespace FluentApiTuto.Part2.Tests
{
    [TestFixture]
    public class ShirtTests
    {
        [Test]
        public void T001_IsBlackByDefault()
        {
            // Given

            // When
            var shirt = new Shirt();
            // Then
            Assert.That(shirt.Color == Color.Black);
        }

        [Test]
        public void T002_IsOfSizeMByDefault()
        {
            // Given

            // When
            var shirt = new Shirt();
            // Then
            Assert.That(shirt.Size == Size.M);
        }

        [Test]
        public void T003_IsMadeOfCottonByDefault()
        {
            // Given

            // When
            var shirt = new Shirt();
            // Then
            Assert.That(shirt.Fabric == Fabric.Cotton);
        }

        [Test]
        public void T004_HasNoFrontInscriptionByDefault()
        {
            // Given

            // When
            var shirt = new Shirt();
            // Then
            Assert.That(shirt.FrontInscription == string.Empty);
        }

        [Test]
        public void T005_HasNoBackInscriptionByDefault()
        {
            // Given

            // When
            var shirt = new Shirt();
            // Then
            Assert.That(shirt.BackInscription == string.Empty);
        }

        [Test]
        public void T006()
        {
            // Given
            var color = Color.ForestGreen;
            // When
            Shirt shirt = new Shirt();// TODO
            // Then
            Assert.That(shirt.Color == color);
        }

        [Test]
        public void T007()
        {
            // Given
            var size = Size.XXS;
            // When
            Shirt shirt = new Shirt();// TODO
            // Then
            Assert.That(shirt.Size == size);
        }

        [Test]
        public void T008()
        {
            // Given
            var fabric = Fabric.Silk;
            // When
            Shirt shirt = new Shirt();// TODO
            // Then
            Assert.That(shirt.Fabric == fabric);
        }

        [Test]
        public void T009()
        {
            // Given
            var frontInscription = "FP";
            // When
            Shirt shirt = new Shirt();// TODO
            // Then
            Assert.That(shirt.FrontInscription == frontInscription);
        }

        [Test]
        public void T010()
        {
            // Given
            var backInscription = "SS";
            // When
            Shirt shirt = new Shirt();// TODO
            // Then
            Assert.That(shirt.BackInscription == backInscription);
        }

        [Test]
        public void T011()
        {
            // Given
            var size = Size.L;
            var fabric = Fabric.Polycotton;
            // When
            Shirt shirt = new Shirt();// TODO
            // Then
            Assert.That(shirt.Size == size);
            Assert.That(shirt.Fabric == fabric);
        }

        [Test]
        public void T012()
        {
            // Given
            var color = Color.Azure;
            var backInscription = "interesting barcamp";
            // When
            Shirt shirt = new Shirt();// TODO
            // Then
            Assert.That(shirt.Color == color);
            Assert.That(shirt.BackInscription == backInscription);
        }

        [Test]
        public void T013()
        {
            // Given
            var size = Size.XS;
            var fabric = Fabric.Flax;
            var frontInscription = "Working at FP";
            var backInscription = "It's gonna change soon";
            // When
            Shirt shirt = new Shirt();// TODO
            // Then
            Assert.That(shirt.Size == size);
            Assert.That(shirt.Fabric == fabric);
            Assert.That(shirt.FrontInscription == frontInscription);
            Assert.That(shirt.BackInscription == backInscription);
        }
    }
}
