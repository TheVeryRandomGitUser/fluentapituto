﻿using System;
using System.Collections.Generic;
using FluentApiTuto.Part1;

namespace FluentApiTuto.Part3
{
    public interface ICompany
    {
        ICompanyFluentApi FromRooms { get; }
    }

    public interface ICompanyFluentApi
    {
        ICompanyFluentApi Take { get; }

        IRoom One();
        IEnumerable<IRoom> Many(int quantity);
    }

    public interface IRoom
    {
        IRoomBookingItFluentApi BookingIt { get; }
    }

    public interface IRoomBookingItFluentApi
    {
        IRoomForFluentApi For(Person person);
        IRoomFromFluentApi From(DateTime bookedFrom);
    }

    public interface IRoomForFluentApi
    {
        IRoomForFluentApi Including { get; }
        IRoomForFluentApi Friends();
        IRoomForFluentApi Relatives();

        IRoomFromFluentApi From(DateTime bookedFrom);
    }

    public interface IRoomFromFluentApi
    {
        IRoomFromFluentApi To(DateTime bookedTo);
        IRoomFromFluentApi For(TimeSpan duration);

        IRoomForFluentApi For(Person person);
    }
}