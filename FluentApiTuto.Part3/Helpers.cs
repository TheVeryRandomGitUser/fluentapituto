﻿using System.Collections.Generic;
using FakeItEasy;

namespace FluentApiTuto.Part3
{
    public static class Helpers
    {
        public static IRoomBookingItFluentApi BookingThem(this IEnumerable<IRoom> rooms)
        {
            return A.Fake<IRoomBookingItFluentApi>();
        }
    }
}