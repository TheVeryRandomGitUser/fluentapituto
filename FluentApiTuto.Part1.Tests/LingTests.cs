﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using NUnit.Framework;

namespace FluentApiTuto.Part1.Tests
{
    [TestFixture]
    public class LingTests
    {
        [Test]
        public void T001_Where()
        {
            // Given
            var input = new[] { 1, 7, 3, 2, 6, 8, 5 };
            var expected = new[] { 2, 6, 8 };
            // When
            var even = input
                .Where(x => x % 2 == 0)
                .ToArray();
            // Then
            CollectionAssert.AreEquivalent(expected, even);
        }

        [Test]
        public void T002_Select()
        {
            // Given
            var input = new[] { Some.Person, Some.Person, Some.Person };
            var expected = new[] { "Some", "Some", "Some" };
            // When
            var firstNames = input
                .Select(x => x.FirstName)
                .ToArray();
            // Then
            CollectionAssert.AreEquivalent(expected, firstNames);
        }

        [Test]
        public void T003_SelectAsConvert()
        {
            // Given
            var input = new double[] { 1, 2, 3, 4 };
            var expected = new[]
            {
                new Complex(0, 1),
                new Complex(0, 2),
                new Complex(0, 3),
                new Complex(0, 4),
            };
            // When
            var complexes = input
                .Select(x => new Complex(0, x))
                .ToArray();
            // Then
            CollectionAssert.AreEquivalent(expected, complexes);
        }

        [Test]
        public void T004_OrderBy()
        {
            // Given
            var input = new[] { 3, 4, 2, 1 }
                .Select(i => new Person().With.FirstName($"{i}").ToPerson)
                .ToArray();
            var expected = new[] { "1", "2", "3", "4" };
            // When
            var orderedFirstNames = input
                .OrderBy(x => x.FirstName)
                .Select(x => x.FirstName)
                .ToArray();
            // Then
            CollectionAssert.AreEqual(expected, orderedFirstNames);
        }

        [Test]
        public void T005_Any()
        {
            // Given
            var input = Enumerable.Empty<Person>();
            // When
            var isNotEmpty = input.Any();
            // Then
            Assert.IsFalse(isNotEmpty);
        }

        [Test]
        public void T006_All()
        {
            // Given
            var input = new[] { 15, -28, 44, 666, -123 };
            // When
            var areAllMultiDigit = input
                .All(x => Math.Abs(x) >= 10);
            // Then
            Assert.IsTrue(areAllMultiDigit);
        }

        [Test]
        public void T007_Intersect()
        {
            // Given
            var first = new[] { 1, 3, 5, 6, 8 };
            var second = new[] { 7, 6, 4, 3, 9 };
            var expected = new[] { 3, 6 };
            // When
            var intersect = first.Intersect(second)
                .ToArray();
            // Then
            CollectionAssert.AreEquivalent(expected, intersect);
        }

        [Test]
        public void T008_TakeSkip()
        {
            // Given
            var input = new[] { 1, 2, 3, 4, 5, 6, 7, 8 };
            var expected = new[] { 3, 4, 5 };
            // When
            var actual = input.Skip(2).Take(3).ToArray();
            // Then
            CollectionAssert.AreEquivalent(expected, actual);
        }

        [Test]
        public void T009()
        {
            // Given
            var input = new[] { 0, 1, -2, 3, -4, 5, -6, 7, -8 };
            var expected = new[] { 11, 13, 15, 17 };
            /*
             * Positive numbers
             * increased by 10
             */
            // When
            var actual = Enumerable.Empty<int>();// TODO
            // Then
            CollectionAssert.AreEquivalent(expected, actual);
        }

        [Test]
        public void T010()
        {
            // Given
            var people = new Person[]
            {
                new Person().With.FirstName("Antonio").LastName("Arevalo"),
                new Person().With.FirstName("William").LastName("Waters"),
                new Person().With.FirstName("Richard").LastName("Ross"),
                new Person().With.FirstName("Uwe").LastName("Upton"),
                new Person().With.FirstName("Kerri").LastName("Kim")
            };
            var expected = new[] { "AntonioArevalo", "WilliamWaters" };
            /*
             * Concatenated first and last names
             * of 2 first people
             */
            // When
            var actual = Enumerable.Empty<string>();// TODO
            // Then
            CollectionAssert.AreEquivalent(expected, actual);
        }

        [Test]
        public void T011()
        {
            // Given
            var people = new Person[]
            {
                new Person()
                    .With.FirstName("Antonio").LastName("Arevalo")
                    .Having.Friend(Some.Person),
                new Person().With.FirstName("William").LastName("Waters")
                    .Having.Friend(Some.Person).Friend(Some.Person),
                new Person().With.FirstName("Richard").LastName("Ross"),
                new Person().With.FirstName("Uwe").LastName("Upton"),
                new Person().With.FirstName("Kerri").LastName("Kim")
                    .Having.Friend(Some.Person).Friend(Some.Person)
            };
            var expected = new[] { "RR", "UU" };
            /*
             * Initials (use person.Initials()) of 
             * forever alone (use person.HasNoFirends())
             * people
             */
            // When
            var actual = Enumerable.Empty<string>();// TODO
            // Then
            CollectionAssert.AreEquivalent(expected, actual);
        }

        [Test]
        public void T012()
        {
            // Given
            var people = new Person[]
            {
                new Person().With.FirstName("Antonio").LastName("Arevalo"),
                new Person().With.FirstName("William").LastName("Waters"),
                new Person().With.FirstName("XXX").LastName("InvalidOperationException"),
                new Person().With.FirstName("XXX").LastName("IOE"),
                new Person().With.FirstName("Richard").LastName("Ross"),
                new Person().With.FirstName("Uwe").LastName("Upton"),
                new Person().With.FirstName("XXX").LastName("ArgumentOutOfRangeException"),
                new Person().With.FirstName("Kerri").LastName("Kim"), 
            };
            var expected = new[] { "Arevalo", "Upton", "Waters" };
            /*
             * Ordered 
             * at least 5 character long
             * surnames of people 
             * whose names are not equal to "XXX"
             */
            // When
            var surnames = Enumerable.Empty<string>();// TODO
            // Then
            CollectionAssert.AreEqual(expected, surnames);
        }

        [Test]
        public void T013()
        {
            // Given
            var A = Some.Person;
            var B = Some.Person;
            var C = Some.Person;
            var people = new Person[]
            {
                new Person()
                    .With.FirstName("Antonio").LastName("Arevalo")
                    .Having.Friend(A),

                new Person().With.FirstName("William").LastName("Waters")
                    .Having.Friend(A).Friend(B)
                    .Relative(C),

                new Person().With.FirstName("Richard").LastName("Ross")
                    .Having.Friend(B).Friend(A)
                    .Relative(B),

                new Person().With.FirstName("Uwe").LastName("Upton"),
                    
                new Person().With.FirstName("Kerri").LastName("Kim")
                    .Having.Friend(A).Friend(B).Friend(C)
                    .Relative(A).Relative(B).Relative(C),
            };
            var expected = new[] { "Richard", "Kerri" };
            /*
             * Names of people
             * having at least one relative
             * being also their friend
             */
            // When
            var actual = Enumerable.Empty<string>();// TODO
            // Then
            CollectionAssert.AreEquivalent(expected, actual);
        }

        [Test]
        public void T014_FizzBuzz()
        {
            // https://en.wikipedia.org/wiki/Fizz_buzz
            // Given
            var fizzConditions = FizzConditions();
            var buzzConditions = BuzzConditions();
            Func<int, bool, bool, string> createFizzBuzzEntry = (number, fizzCondition, buzzCondition) =>
            {
                return !fizzCondition && !buzzCondition
                    ? number.ToString()
                    : $"{(fizzCondition ? "Fizz" : "")}{(buzzCondition ? "Buzz" : "")}";
            };
            var expected = new string[15]
            {
                "1",
                "2",
                "Fizz",
                "4",
                "Buzz",
                "Fizz",
                "7",
                "8",
                "Fizz",
                "Buzz",
                "11",
                "Fizz",
                "13",
                "14",
                "FizzBuzz",
            };
            // When
            var actual = Enumerable.Empty<string>();// TODO
            // Then
            CollectionAssert.AreEqual(expected, actual);
        }

        private static IEnumerable<bool> FizzConditions()
        {
            var cycle = new[] { false, false, true };
            while (true)
                foreach (var fizzCondition in cycle)
                    yield return fizzCondition;
        }

        private static IEnumerable<bool> BuzzConditions()
        {
            var cycle = new[] { false, false, false, false, true };
            while (true)
                foreach (var buzzCondition in cycle)
                    yield return buzzCondition;
        }
    }
}