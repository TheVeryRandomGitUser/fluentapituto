﻿using System.Linq;

namespace FluentApiTuto.Part1.Tests
{
    public static class Some
    {
        public static Person Person => new Person().With.FirstName("Some").LastName("Some").ToPerson;
    }

    public static class PersonExtension
    {
        public static bool HasNoFirends(this Person person)
        {
            return !person.Friends.Any();
        }

        public static string Initials(this Person person)
        {
            return $"{person.FirstName[0]}{person.LastName[0]}";
        }
    }
}
