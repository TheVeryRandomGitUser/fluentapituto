﻿using System.Linq;
using NUnit.Framework;

namespace FluentApiTuto.Part1.Tests
{
    [TestFixture]
    public class PersonTests
    {
        [Test]
        public void T001_NormalCtor()
        {
            // Given

            // When
            var person = new Person(
                "Mateusz", 
                "Drażyk", 
                new[] { Some.Person, Some.Person }, 
                Enumerable.Empty<Person>());
            // Then
            Assert.IsTrue(person.HasNoFirends());
        }

        [Test]
        public void T002_ObjectInitialization()
        {
            // Given

            // When
            var person = new Person
            {
                FirstName = "Mateusz",
                LastName = "Drażyk",
            };
            foreach (var relative in new[] { Some.Person, Some.Person })
            {
                person.Relatives.Add(relative);
            }
            // Then
            Assert.IsTrue(person.HasNoFirends());
        }

        [Test]
        public void T003_FluentApi()
        {
            // Given

            // When
            var person = new Person()
                .With.FirstName("Mateusz").LastName("Drażyk")
                .And
                .Having.Relative(Some.Person).Relative(Some.Person)
                .ToPerson;
            // Then
            Assert.IsTrue(person.HasNoFirends());
        }

        [Test]
        public void T004_FluentApiWithoutEscapeWord()
        {
            // Given

            // When
            var person = new Person()
                .With.FirstName("Mateusz").LastName("Drażyk")
                //.And
                .Having.Relative(Some.Person).Relative(Some.Person)//.Relatives(new[] { Some.Person, Some.Person })
                .ToPerson;
            // Then
            Assert.IsTrue(person.HasNoFirends());
        }

        [Test]
        public void T005_FluentApiImplicitConversion()
        {
            // Given
            Person person;
            // When
            person = new Person()
                .With.FirstName("Mateusz").LastName("Drażyk");
            // Then
            Assert.That(person is Person);
        }

        [Test]
        public void T006_FluentApiImplicitConversion()
        {
            // Given

            // When
            var person = new Person()
                .With.FirstName("Mateusz").LastName("Drażyk");
            // Then
            Assert.That(person is Person);
        }

        [Test]
        public void T007_FluentApliImplicitConversion()
        {
            // Given

            // When
            var people = new[]
            {
                new Person().With.FirstName("Antonio").LastName("Arevalo"),
                new Person().With.FirstName("William").LastName("Waters"),
                new Person().With.FirstName("Richard").LastName("Ross"),
                new Person().With.FirstName("Uwe").LastName("Upton"),
                new Person().With.FirstName("Kerri").LastName("Kim")
            };
            // Then
            Assert.That(people.All(person => person is Person));
        }
    }
}
