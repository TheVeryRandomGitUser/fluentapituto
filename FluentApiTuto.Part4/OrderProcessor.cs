﻿using FluentApiTuto.Part4.Services;

namespace FluentApiTuto.Part4
{
    public class OrderProcessor
    {
        private readonly IStorageService storageService;
        private readonly IShippingService shippingService;
        private readonly IAuditService auditService;

        public OrderProcessor(IStorageService storageService, IShippingService shippingService, IAuditService auditService)
        {
            this.storageService = storageService;
            this.shippingService = shippingService;
            this.auditService = auditService;
        }

        public void Process(Order order)
        {
            // TODO
        }
    }
}