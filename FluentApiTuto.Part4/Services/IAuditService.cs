﻿namespace FluentApiTuto.Part4.Services
{
    public interface IAuditService
    {
        void NotifySent(IPackage package);
        void NotifyInsured(IPackage package);
    }
}