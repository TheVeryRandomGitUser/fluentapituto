﻿using FluentApiTuto.Part2;

namespace FluentApiTuto.Part4.Services
{
    public interface IStorageService
    {
        IPackage Fetch(IProduct product, int quantity);
    }
}