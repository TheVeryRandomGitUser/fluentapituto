﻿namespace FluentApiTuto.Part4.Services
{
    public interface IShippingService
    {
        void Ship(IPackage package, ShippingMethod method);
    }
}