﻿namespace FluentApiTuto.Part4
{
    public enum ShippingMethod
    {
        Normal,
        Express,
        Courier,
    }
}