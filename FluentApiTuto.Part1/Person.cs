﻿using System.Collections.Generic;
using System.Linq;

namespace FluentApiTuto.Part1
{
    public class Person
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public IList<Person> Relatives { get; } = new List<Person>();
        public IList<Person> Friends { get; } = new List<Person>();

        public Person()
        {
        }

        public Person(
            string firstName, 
            string lastName, 
            IEnumerable<Person> relatives, 
            IEnumerable<Person> friends)
        {
            FirstName = firstName;
            LastName = lastName;
            Relatives = relatives.ToList();
            Friends = friends.ToList();
        }

        private PersonFluentApiWith with;
        public PersonFluentApiWith With => with ?? (with = new PersonFluentApiWith(this));

        private PersonFluentApiHaving having;
        public PersonFluentApiHaving Having => having ?? (having = new PersonFluentApiHaving(this));
    }

    public class PersonFluentApiWith
    {
        private readonly Person person;

        public PersonFluentApiWith(Person person)
        {
            this.person = person;
        }

        public PersonFluentApiWith FirstName(string firstName)
        {
            person.FirstName = firstName;
            return this;
        }

        public PersonFluentApiWith LastName(string lastName)
        {
            person.LastName = lastName;
            return this;
        }

        public Person And => person;
        public PersonFluentApiHaving Having => person.Having;
        public Person ToPerson => person;

        public static implicit operator Person(PersonFluentApiWith personFluentApiWith)
        {
            return personFluentApiWith.person;
        }
    }

    public class PersonFluentApiHaving
    {
        private readonly Person person;

        public PersonFluentApiHaving(Person person)
        {
            this.person = person;
        }

        public PersonFluentApiHaving Relative(Person relative)
        {
            person.Relatives.Add(relative);
            return this;
        }

        public PersonFluentApiHaving Relatives(IEnumerable<Person> relatives)
        {
            foreach (var relative in relatives)
                person.Relatives.Add(relative);
            return this;
        }

        public PersonFluentApiHaving Friend(Person friend)
        {
            person.Friends.Add(friend);
            return this;
        }

        public PersonFluentApiHaving Friends(IEnumerable<Person> friends)
        {
            foreach (var friend in friends)
                person.Friends.Add(friend);
            return this;
        }

        public Person And => person;
        public PersonFluentApiWith With => person.With;
        public Person ToPerson => person;

        public static implicit operator Person(PersonFluentApiHaving personFluentApiHaving)
        {
            return personFluentApiHaving.person;
        }
    }
}
