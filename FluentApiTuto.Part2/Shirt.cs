﻿using System.Drawing;

namespace FluentApiTuto.Part2
{
    public class Shirt : IProduct
    {
        public Color Color { get; }
        public Size Size { get; }
        public Fabric Fabric { get; }
        public string FrontInscription { get; }
        public string BackInscription { get; }

        // TODO
    }
}