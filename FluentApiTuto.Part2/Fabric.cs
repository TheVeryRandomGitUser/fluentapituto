﻿namespace FluentApiTuto.Part2
{
    public enum Fabric
    {
        Polyester,
        Polycotton,
        Cotton,
        Flax,
        Silk
    }
}