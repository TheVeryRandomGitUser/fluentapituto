﻿namespace FluentApiTuto.Part2
{
    public enum Size
    {
        XXS,
        XS,
        S,
        M,
        L,
        XL,
        XXL
    }
}