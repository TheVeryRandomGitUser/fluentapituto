﻿using System;
using FakeItEasy;
using FluentApiTuto.Part1;
using NUnit.Framework;

namespace FluentApiTuto.Part3.Tests
{
    [TestFixture]
    public class CompanyTests
    {
        [Test]
        public void T001_GettingRoom()
        {
            // Given
            var company = A.Fake<ICompany>();
            // When
            var room = company
                .FromRooms.Take.One();
            var rooms = company
                .FromRooms.Take.Many(7);
            // Then
        }

        [Test]
        public void T002_BookingRoom()
        {
            // Given
            var person = new Person();
            var company = A.Fake<ICompany>();
            // When
            company
                .FromRooms.Take.One()
                .BookingIt.For(person);
            // Then
        }

        [Test]
        public void T003_BookingRoom()
        {
            // Given
            var company = A.Fake<ICompany>();
            // When
            company
                .FromRooms.Take.One()
                .BookingIt.From(DateTime.Now).To(DateTime.MaxValue);
            // Then
        }

        [Test]
        public void T004_BookingRoom()
        {
            // Given
            var person = new Person();
            var company = A.Fake<ICompany>();
            // When
            company
                .FromRooms.Take.One()
                .BookingIt
                    .For(person).Including.Relatives()
                    .From(DateTime.Now).To(DateTime.MaxValue);
            // Then
        }

        [Test]
        public void T005_BookingRoom()
        {
            // Given
            var person = new Person();
            var company = A.Fake<ICompany>();
            // When
            company
                .FromRooms.Take.One()
                .BookingIt
                    .From(DateTime.Now).For(TimeSpan.FromHours(8))
                    .For(person).Including.Friends();
            // Then
        }

        [Test]
        public void T006_BookingRooms()
        {
            // Given
            var person = new Person();
            var company = A.Fake<ICompany>();
            // When
            company
                .FromRooms.Take.Many(3)
                .BookingThem()
                    .From(DateTime.Now).For(TimeSpan.FromHours(8))
                    .For(person).Including.Friends();
            // Then
        }
    }
}